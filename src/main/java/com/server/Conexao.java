package com.server;
import java.sql.Connection;  
import java.sql.DriverManager;  
import java.sql.SQLException;  

//import javax.swing.JOptionPane;  
 
public class Conexao {  
  final private String url = "jdbc:postgresql://localhost:5432/senhas";  
  final private String usuario = "gerente"; 
  final private String senha = "123";  
  final private String driver = "org.postgresql.Driver";
  
  Connection con;
  
  public Connection conexao() {  
    try {  
      Class.forName(driver);  
    } catch (ClassNotFoundException cnfe) {  
      //JOptionPane.showMessageDialog(null, "n�o encontrado");  
      System.out.println("Driver n�o encontrado!!");  
      cnfe.printStackTrace();  
    }  
    try {  
      con = DriverManager.getConnection(url, usuario, senha);  
      //Conseguiu conectar...  
    } catch (SQLException se) {  
      System.out.println("N�o foi possivel conectar");  
      se.printStackTrace();  
    }  
    return con;  
  }  
}  
